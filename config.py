picture_config = {
    'picture_width': 400,
    'picture_length': 350,
    'picture_color': "white",
    'picture_type': "RGB"
}
font_config = {
    'title_font_size': 30,
    'content_font_size': 16,
    'title_fillcolor': 'black',
    'content_fillcolor': 'black'
}
style_config = {
    'title_x': 75,
    'title_y': 25,
    'content_title_start_x': 5,
    'content_title_start_y': 75,
    'content_data_x': 85,
    'content_data_y': 75,
    'second_title': ['下单时间', '支付时间', '订单号码', '商品名称', '商品数量', '支付金额'],

}
