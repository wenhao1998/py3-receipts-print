from PIL import Image, ImageDraw, ImageFont
from config import picture_config, font_config, style_config


def spanned_file():
    picture_width = picture_config['picture_width']  # 图片宽度
    picture_length = picture_config['picture_length']  # 图片长度
    picture_color = picture_config['picture_color']  # 图片颜色
    picture_type = picture_config['picture_type']  # 图片类型
    title_font_size = font_config['title_font_size']  # 标题字体大小
    content_font_size = font_config['content_font_size']  # 内容字体大小
    title_fillcolor = font_config['title_fillcolor']  # 标题字体颜色
    content_fillcolor = font_config['content_fillcolor']  # 内容字体颜色
    title_x = style_config['title_x']  # 标题坐标x轴
    title_y = style_config['title_y']  # 标题坐标y轴
    content_title_start_x = style_config['content_title_start_x']  # 内容标题初始坐标轴x
    content_title_start_y = style_config['content_title_start_y']  # 内容标题初始坐标轴y
    content_data_x = style_config['content_data_x']  # 内容初始坐标轴x
    content_data_y = style_config['content_data_y']  # 内容初始坐标轴y
    second_title = style_config['second_title']  # 二级标题列表
    # 创建空白图片
    image = Image.new(picture_type, (picture_width, picture_length), picture_color)
    draw = ImageDraw.Draw(image)
    # 设置字体样式及大小
    title_myfont = ImageFont.truetype(u'simsun.ttc', size=title_font_size)
    content_myfont = ImageFont.truetype(u'simsun.ttc', size=content_font_size)
    # 写入标题
    draw.text((title_x, title_y), u"".join('上海酒云研究所'), font=title_myfont, fill=title_fillcolor)
    x = content_title_start_x
    y = content_title_start_y
    # 循环写入小标题
    for per in second_title:
        draw.text((x, y), u"".join('{}：'.format(per)), font=content_myfont, fill=content_fillcolor)
        y = y + 40
    x1 = content_data_x
    y1 = content_data_y
    # 循环填入数据
    for value in ('2022-03-03 12:00:00', '2022-03-03 12:00:00', 'OSMS20220311223232', 'XXXXXX咖啡', 1, '￥88.00'):
        draw.text((x1, y1), u"".join('{}'.format(value)), font=content_myfont, fill=content_fillcolor)
        y1 = y1 + 40
    image.save('小票.png')


spanned_file()
